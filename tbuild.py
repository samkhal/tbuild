#!/use/bin/env python3

import subprocess
import re
from pathlib import Path
from collections import namedtuple, defaultdict

compiler = "g++"
# compile_flags = ["-std=c++11", "-Wl,--unresolved-symbols,ignore-all"]
compile_flags = ["-std=c++11", "-Wl,--warn-unresolved-symbols"]


def run_command(cmd, **kwargs):
    print_str = " ".join(cmd) if isinstance(cmd, list) else cmd
    print("Executing:", print_str, flush=True)
    return subprocess.run(cmd, **kwargs)


def get_export_strings(filepath):
    exports = []
    with filepath.open() as f:
        for line in f:
            match = re.search("EXPORT_TEMPLATE_FUN\((.*)\);?\s*$", line)
            if match:
                exports.append(match.group(1))

    return exports


def get_exported_symbols(filepath, builddir):
    export_strings = get_export_strings(filepath)
    print(export_strings)

    if not export_strings:
        return []

    exportfile = """
# include "{includefile}"
struct EXPORT_TYPE_T{{ }} EXPORT_TYPE;
void template_export_dummy(){{
    {body}
}}
""".format(
        includefile=filepath.stem+".h",
        body="\n".join([st+";" for st in export_strings])
    )

    exports_sourcepath = builddir/filepath.name
    exports_opath = exports_sourcepath
    with exports_sourcepath.open("w") as f:
        f.write(exportfile)

    includepath = filepath.parent
    exports_opath_str = str(exports_opath.parent/filepath.stem) + ".o"
    run_command([compiler, *compile_flags, "-I"+str(includepath), "-c",
                 "-o", exports_opath_str, str(exports_sourcepath)], check=True)

    result = run_command(["nm", "-C", exports_opath_str],
                         stdout=subprocess.PIPE, check=True)
    nm_out = result.stdout.decode('utf-8')
    export_names = []
    for line in nm_out.split("\n"):
        if "EXPORT_TYPE_T" in line:
            components = line.split()
            assert components[0] == "U"
            export_names.append(components[2].split('<')[0])

    return export_names


Symbol = namedtuple("Symbol", "name kind")


def get_symbols(filepath, dynamic=False):
    if dynamic:
        cmd = ["nm", "--dynamic", str(filepath)]
    else:
        cmd = ["nm", str(filepath)]

    result = run_command(cmd,
                         check=True, stdout=subprocess.PIPE)
    nm_out = result.stdout.decode('utf-8')
    syms = []
    for line in nm_out.split("\n"):
        if line:
            syms.append(Symbol(line[19:], line[17]))
    return syms


def demangle(symbol_name):
    result = run_command(["c++filt", symbol_name],
                         check=True, stdout=subprocess.PIPE)
    return result.stdout.decode('utf-8').strip()


def get_undefined_symbols(filepath):
    dynamic_syms = set(get_symbols(filepath, dynamic=True))
    all_syms = set(get_symbols(filepath))

    nondynamic_syms = all_syms - dynamic_syms

    undefined_syms = [sym.name for sym in nondynamic_syms if sym.kind == "U"]

    return undefined_syms

    # for each compile step:
    #     Scan cpp file for EXPORT_TEMPLATE clause
    #     Create a cpp file which:
    #         includes the corresponding header
    #         uses the forward-declared symbol
    #     Compile the export file into an obj
    #     Extract the name of the exported symbol
    #     Store exported symbol in map of names->inls

    # for each link step:
    #     Link, but allow undefined symbols to remain
    #     Extract all undefined symbols
    #     For each undefined name:
    #         Check if name exists in map of names->instantated objs:
    #             if exists, add
    #         Check if name exists in map of names->inls
    #             If no, fail
    #         If yes:
    #             Create a cpp file which:
    #                 includes the inl (and other needed headers?)
    #                 instantiates the template
    #             Compile to an obj
    #             Add obj to


def instantiate_template(symbol, target, config):
    inlfile_path = config.get_target_inlfile(target)

    instantiation_file = """
# include "{inlfile}"
template {demangled_symbol};
""".format(
        inlfile=str(inlfile_path.name),
        demangled_symbol=demangle(symbol)
    )

    filepath = (config.build_dir/symbol).with_suffix(".cpp")
    with filepath.open("w") as f:
        f.write(instantiation_file)

    includepath = inlfile_path.parent
    ofile_path = filepath.with_suffix(".o")
    run_command([compiler, *compile_flags, "-I"+str(includepath), "-c",
                 "-o", str(ofile_path), str(filepath)], check=True)

    return symbol


def get_name(symbol):
    demangled = demangle(symbol)
    m = re.search("(\w*)(?=\(|<|$)", demangled)
    return m.group(0)


class BuildConfig():
    def __init__(self, config, build_dir):
        self.config = config
        self.build_dir = Path(build_dir)
        self.exports = defaultdict(set)
        self.targets = {}  # map of targets to source files

    def get_target_ofile(self, target):
        return (self.build_dir/target).with_suffix(".o")

    def get_target_binfile(self, target):
        return self.build_dir/target

    def get_target_inlfile(self, target):
        sourcefile = self.targets[target]
        return sourcefile.with_suffix(".inl")

    def build(self):
        for target in self.config:
            target.run(self)

    def run(self, target, **kwargs):
        return run_command([str(self.get_target_binfile(target))], **kwargs)

    def add_exported_symbols(self, target, exported_symbols):
        for symbol in exported_symbols:
            self.exports[get_name(symbol)].add(target)

    def add_target(self, target, sourcepath):
        assert target not in self.targets
        self.targets[target] = sourcepath


class Compile:
    def __init__(self, output_target, input_file):
        self.output_target = output_target
        self.input_path = Path(input_file)

    def run(self, config):
        config.add_target(self.output_target, self.input_path)
        exports = get_exported_symbols(self.input_path, config.build_dir)
        config.add_exported_symbols(self.output_target, exports)
        run_command([compiler, *compile_flags, "-c",
                     "-o", str(config.get_target_ofile(self.output_target)), str(self.input_path)], check=True)


class Link:
    def __init__(self, output_target, input_targets):
        self.output_target = output_target
        self.input_targets = input_targets

    def run(self, config):
        while True:
            run_command([compiler, *compile_flags, "-o",
                         str(config.get_target_binfile(self.output_target)),
                         *[str(config.get_target_ofile(in_targ)) for in_targ in self.input_targets]], check=True)

            undefined_symbols = get_undefined_symbols(
                config.get_target_binfile(self.output_target))

            if not undefined_symbols:
                break

            print("Undefined Symbols!", undefined_symbols)

            print(config.exports)
            for sym in undefined_symbols:
                print("Name:", get_name(sym))
                if get_name(sym) in config.exports:
                    targets = config.exports[get_name(sym)]
                    print("Found", len(targets), "targets")
                    for target in targets:
                        new_target = instantiate_template(sym, target, config)
                        self.input_targets.append(new_target)
                else:
                    print(config.exports)
                    raise RuntimeError("Could not find name "+get_name(sym))
