#include "lib2.h"
#include "lib.h"
#include <iostream>

template <class T>
void simpleFun2(T t)
{
    simpleFun(t);
    std::cout << "Goodbye" << std::endl;
    return;
}