#include "lib.h"
#include <iostream>

template <class T>
void simpleFun(T t)
{
    std::cout << "Hello" << std::endl;
    return;
}

template <class T>
void simpleFun2(T t)
{
    std::cout << "Goodbye" << std::endl;
    return;
}