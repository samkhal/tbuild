from .. import tbuild
import subprocess
from pathlib import Path


def test_extract():
    strings = tbuild.get_export_strings(Path("test/simple/lib.cpp"))
    assert strings == ["simpleFun(EXPORT_TYPE)"]


def test_syms(tmp_path):
    sym_names = tbuild.get_exported_symbols(
        Path("test/simple/lib.cpp"), tmp_path)
    assert sym_names == ["simpleFun"]


Compile = tbuild.Compile
Link = tbuild.Link


def test_normal_build(tmp_path):
    targets = [
        Compile("lib", "test/normal/lib.cpp"),
        Compile("main", "test/normal/main.cpp"),
        Link("out", ["main", "lib"])
    ]

    buildconfig = tbuild.BuildConfig(targets, str(tmp_path))
    buildconfig.build()
    result = buildconfig.run("out", stdout=subprocess.PIPE)
    assert result.stdout.decode('utf-8') == "Hello\n"


def test_undefined_symbols(tmp_path):
    targets = [
        Compile("main", "test/normal/main.cpp"),
    ]

    buildconfig = tbuild.BuildConfig(targets, str(tmp_path))
    buildconfig.build()
    assert [tbuild.demangle(sym) for sym in tbuild.get_undefined_symbols(
        tmp_path/"main.o")] == ["fun()"]


def test_undefined_names_bin(tmp_path):
    targets = [
        Compile("lib", "test/normal/lib.cpp"),
        Compile("main", "test/normal/main.cpp"),
        Link("out", ["main", "lib"])
    ]

    buildconfig = tbuild.BuildConfig(targets, str(tmp_path))
    buildconfig.build()
    assert tbuild.get_undefined_symbols(tmp_path/"out") == []


def test_get_name():
    assert tbuild.get_name("void fun()") == "fun"
    assert tbuild.get_name("void fun<Blah>()") == "fun"
    assert tbuild.get_name("fun()") == "fun"


def test_tbuild(tmp_path):
    targets = [
        Compile("lib", "test/simple/lib.cpp"),
        Compile("main", "test/simple/main.cpp"),
        Link("out", ["main", "lib"])
    ]

    buildconfig = tbuild.BuildConfig(targets, str(tmp_path))
    buildconfig.build()
    result = buildconfig.run("out", stdout=subprocess.PIPE, check=True)
    assert result.stdout.decode('utf-8') == "Hello\n"


def test_multistep(tmp_path):
    targets = [
        Compile("lib", "test/multistep/lib.cpp"),
        Compile("main", "test/multistep/main.cpp"),
        Link("out", ["main", "lib"])
    ]

    buildconfig = tbuild.BuildConfig(targets, str(tmp_path))
    buildconfig.build()
    result = buildconfig.run("out", stdout=subprocess.PIPE, check=True)
    assert result.stdout.decode('utf-8') == "Hello\nGoodbye\n"


def test_multistep2(tmp_path):
    targets = [
        Compile("lib", "test/multistep2/lib.cpp"),
        Compile("lib2", "test/multistep2/lib2.cpp"),
        Compile("main", "test/multistep2/main.cpp"),
        Link("out", ["main", "lib", "lib2"])
    ]

    buildconfig = tbuild.BuildConfig(targets, str(tmp_path))
    buildconfig.build()
    result = buildconfig.run("out", stdout=subprocess.PIPE)
    assert result.stdout.decode('utf-8') == "Hello\nGoodbye\n"
